import React from 'react';

import whatsappIcon from '../../assets/images/icons/whatsapp.svg';

import PageHeader from '../../components/PageHeader';

import './styles.css';

function TeacherItem(){
    return (
        <article className="teacher-item">
        <header> 
            <img src="https://avatars2.githubusercontent.com/u/15962189?s=460&u=34cbf18640c06b6ba5c5924443b9a42e86004a51&v=4" alt="Railander"/>
             <div>
                 <strong>Railander</strong>
                 <span>Big Data</span>
             </div>
        </header>

        <p>
         Analisar dados de forma correta pode ser o fator determinante para decidir se uma empresa atinge resultados medianos ou significativos.
         <br/><br/>
         Nesse sentido, ser capaz de compreender o fluxo de dados da sua empresa garante que as decisões tomadas serão sempre ágeis, confiáveis e assertivas.</p>

         <footer>
             <p>
                 Preço/Hora
                 <strong>R$ 80,00</strong>   
             </p>
             <button type="button">
                 <img src={whatsappIcon} alt="Whatsapp"/>
                 Entra em contato
             </button>
         </footer>
 </article>
    )
}

export default TeacherItem;